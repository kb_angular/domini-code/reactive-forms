import { Component } from '@angular/core';
// FormControl -> para trabajar en este componente con formularios reactivos
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent  {

  name = new FormControl('');

}
